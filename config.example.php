<?php
/*
 * Define the configuration for gitlab-webhook-php in this file.
 * 
 * $branches is an array containing one array for every branch that should
 * automatically be pulled. This array should contain three key/value pairs:
 *  'repo': the GitLab url for the repo (e.g. 'lkkmpn/gitlab-webhook-php')
 *  'branch': the branch that should be pulled (e.g. 'master')
 *  'dir': the directory on the server where the local Git repo resides
 *   (e.g. '/var/www/gitlab-webhook-php')
 * 
 * $secret_token is a string containing the secret token used to validate
 * payloads from the GitLab webhook. If you do not want to use a secret token,
 * remove this variable or set it to false.
 */

$branches = [
    [
        'repo' => 'lkkmpn/gitlab-webhook-php',
        'branch' => 'master',
        'dir' => '/var/www/gitlab-webhook-php'
    ]
];

$secret_token = 'thisisasecret';

$enable_dashboard = true;
?>
