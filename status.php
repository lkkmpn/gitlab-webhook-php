<?php
require_once 'config.php';

if (!isset($enable_dashboard) || !$enable_dashboard) {
    http_response_code(403);
    die('403 Forbidden');
}

if (!isset($_GET['repo']) && !isset($_GET['branch'])) {
    header('Location: index.php');
}

$repo = $_GET['repo'];
$branch = $_GET['branch'];

$branch_keys = array_intersect(
    array_keys(array_column($branches, 'repo'), $repo),
    array_keys(array_column($branches, 'branch'), $branch)
);

if (count($branch_keys) == 0) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>GitLab webhook status | <?=$repo?>: <?=$branch?></title>
        <link rel="stylesheet" href="style/style.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="script/script.js"></script>
    </head>
    <body>
        <h1>GitLab webhook status</h1>
        <p><a href="index.php">&larr; go back to index</a></p>
        <?php
        foreach($branch_keys as $branch_key) {
            echo '<h2>' . $repo . ': ' . $branch . '</h2>';
            $branch = $branches[$branch_key];

            $status_file = 'repos/' . $branch['repo'] . '/' . $branch['branch'];
            if (file_exists($status_file)) {
                $status = unserialize(file_get_contents($status_file));
                $gitlab_data = json_decode($status['gitlab_request'], true);
                if ($status['dir_exists'] && $status['git_checkout_status'] === 0 && $status['git_pull_status'] === 0) {
                    $icon = 'img/tick.svg';
                    $status_info = sprintf('<span title="%s">%s</span> &rarr; <span title="%s">%s</span>',
                        $status['before'], substr($status['before'], 0, 8), $status['after'], substr($status['after'], 0, 8));
                    
                    // add commit compare link
                    if (isset($gitlab_data['project']['web_url'])) {
                        $compare_url = sprintf('%s/compare/%s...%s',
                            $gitlab_data['project']['web_url'], $status['before'], $status['after']);
                        $status_info = sprintf('<a href="%s">%s</a>', $compare_url, $status_info);
                    }

                    $status_info_class = 'good';
                } else {
                    $icon = 'img/cross.svg';
                    $errors = [];
                    if ($status['git_checkout_status'] !== 0) $errors[] = 'checkout';
                    if ($status['git_pull_status'] !== 0) $errors[] = 'pull';
                    $status_info = implode(' & ', $errors) . ' error';
                    if ($status['dir_exists'] !== 0) $status_info = 'dir doesn\'t exist';
                    $status_info_class = 'bad';
                }
                ?>
        <p><strong>Last request</strong>: <span class="status-info <?=$status_info_class?>"><?=$status_info?> (<span data-time="<?=$status['timestamp']?>"><?=date('r', $status['timestamp'])?></span>)</span></p>
        <p><strong>GitLab request body</strong>:</p>
        <pre><?=$status['gitlab_request']?></pre>
        <p><strong><code>git checkout</code> status</strong>: <?=$status['git_checkout_status']?></p>
        <p><strong><code>git checkout</code> output</strong>:</p>
        <pre><?=implode(PHP_EOL, $status['git_checkout_output'])?></pre>
        <p><strong><code>git pull</code> status</strong>: <?=$status['git_pull_status']?></p>
        <p><strong><code>git pull</code> output</strong>:</p>
        <pre><?=implode(PHP_EOL, $status['git_pull_output'])?></pre>
                <?php
            } else {
                echo '<p>This webhook has never ran.</p>';
            }
        }
        ?>
    </body>
</html>
