<?php
require_once 'config.php';

// check if request is POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(400);
    die('400 Bad Request');
}

// verify secret token if required
if (isset($secret_token) && $secret_token !== false) {
    if (!isset($_SERVER['HTTP_X_GITLAB_TOKEN']) || $_SERVER['HTTP_X_GITLAB_TOKEN'] !== $secret_token) {
        http_response_code(400);
        die('400 Bad Request');
    }
}

// get input from webhook request
$json = file_get_contents('php://input');

$data = json_decode($json, true);

$repo = $data['project']['path_with_namespace'];
$ref = $data['ref'];
$before = $data['before'];
$after = $data['after'];

$ref = explode('/', $ref);
$branch = $ref[count($ref) - 1];

$branch_keys = array_intersect(
    array_keys(array_column($branches, 'repo'), $repo),
    array_keys(array_column($branches, 'branch'), $branch)
);

// GitLab expects a response from the server within 10 seconds [1]. However,
// because the git pull command can take longer to run (depending on the size
// of the commit), we should return a response now, and start pulling
// afterwards. This has the disadvantage that the git pull status cannot be
// returned to GitLab, but it can only be viewed from the status pages from
// this project.
// [1] https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
ob_start();
echo 'Request OK. For details about the pull status, view the status page connected to this webhook endpoint.' . PHP_EOL;
$size = ob_get_length();
header('Content-Encoding: none');
header('Content-Length: ' . $size);
header('Connection: close');
ob_end_flush();
ob_flush();
flush();

$cwd = getcwd();

foreach($branch_keys as $branch_key) {
    $branch_config = $branches[$branch_key];

    unset($git_checkout_output);
    unset($git_pull_output);

    $dir_exists = file_exists($branch_config['dir']);

    if ($dir_exists) {
        chdir($branch_config['dir']);

        exec('git checkout ' . escapeshellcmd($branch), $git_checkout_output, $git_checkout_status);

        if ($git_checkout_status === 0) {
            exec('git pull origin ' . escapeshellcmd($branch), $git_pull_output, $git_pull_status);
        }
    } else {
        $git_checkout_output = false;
        $git_checkout_status = 0;
        $git_pull_output = false;
        $git_pull_status = 0;
    }

    // write status to files
    chdir($cwd);

    $dirname = 'repos/' . $repo . '/';

    if (!file_exists($dirname)) {
        mkdir($dirname, 0777, true);
    }

    $status = serialize([
        'gitlab_request' => $json,
        'dir_exists' => $dir_exists,
        'git_checkout_output' => $git_checkout_output,
        'git_checkout_status' => $git_checkout_status,
        'git_pull_output' => $git_pull_output,
        'git_pull_status' => $git_pull_status,
        'timestamp' => time(),
        'before' => $before,
        'after' => $after
    ]);

    file_put_contents($dirname . $branch, $status);
}
?>
