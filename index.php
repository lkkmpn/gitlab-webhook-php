<?php
require_once 'config.php';

if (!isset($enable_dashboard) || !$enable_dashboard) {
    http_response_code(403);
    die('403 Forbidden');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>GitLab webhook status</title>
        <link rel="stylesheet" href="style/style.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="script/script.js"></script>
    </head>
    <body>
        <h1>GitLab webhook status</h1>
        <ul class="webhooks-list">
            <?php
            foreach($branches as $branch):
                $status_file = 'repos/' . $branch['repo'] . '/' . $branch['branch'];
                if (file_exists($status_file)) {
                    $status = unserialize(file_get_contents($status_file));
                    $gitlab_data = json_decode($status['gitlab_request'], true);
                    
                    // add link to repo
                    if (isset($gitlab_data['project']['web_url'])) {
                        $repo_name = sprintf('<a href="%s">%s</a>', $gitlab_data['project']['web_url'], $branch['repo']);
                    } else {
                        $repo_name = $branch['repo'];
                    }
                    
                    if ($status['dir_exists'] && $status['git_checkout_status'] === 0 && $status['git_pull_status'] === 0) {
                        $icon = 'img/tick.svg';
                        $status_info = sprintf('<span title="%s">%s</span> &rarr; <span title="%s">%s</span>',
                            $status['before'], substr($status['before'], 0, 8), $status['after'], substr($status['after'], 0, 8));
                        
                        // add commit compare link
                        if (isset($gitlab_data['project']['web_url'])) {
                            $compare_url = sprintf('%s/compare/%s...%s',
                                $gitlab_data['project']['web_url'], $status['before'], $status['after']);
                            $status_info = sprintf('<a href="%s">%s</a>', $compare_url, $status_info);
                        }

                        $status_info_class = 'good';
                    } else {
                        $icon = 'img/cross.svg';
                        $errors = [];
                        if ($status['git_checkout_status'] !== 0) $errors[] = 'checkout';
                        if ($status['git_pull_status'] !== 0) $errors[] = 'pull';
                        $status_info = implode(' & ', $errors) . ' error';
                        if ($status['dir_exists'] !== 0) $status_info = 'dir doesn\'t exist';
                        $status_info_class = 'bad';
                    }
                } else {
                    $status = false;
                }
                ?>
            <li>
                <div class="repo-info">
                    <p class="repo-url"><?=$repo_name?>: <?=$branch['branch']?></p>
                    <p class="repo-dir"><?=$branch['dir']?></p>
                </div>
                <div class="repo-status">
                    <?php if ($status): ?>
                    <div class="last-updated">
                        <img class="icon" src="<?=$icon?>">
                        <p class="last-updated">
                            <a href="status.php?repo=<?=urlencode($branch['repo'])?>&branch=<?=urlencode($branch['branch'])?>">
                                <span data-time="<?=$status['timestamp']?>"><?=date('r', $status['timestamp'])?></span>
                            </a>
                        </p>
                    </div>
                    <p class="status-info <?=$status_info_class?>"><?=$status_info?></p>
                    <?php else: ?>
                    <div class="last-updated">never ran</div>
                    <?php endif; ?>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
        <footer>
            <p>
                <a href="https://gitlab.com/lkkmpn/gitlab-webhook-php">view project on GitLab</a> |
                page rendered on <?=date('r')?>
            </p>
        </footer>
    </body>
</html>
