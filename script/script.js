document.addEventListener('DOMContentLoaded', function() {
    const timeElements = Array.from(document.querySelectorAll('[data-time]'));

    timeElements.forEach(function(el) {
        const momentObj = moment.unix(el.getAttribute('data-time'));
        el.innerText = momentObj.fromNow();
        el.setAttribute('title', momentObj.format('MMM D, YYYY H:mm:ss'));
    });
});
