# gitlab-webhook-php

A simple PHP interface for GitLab's webhook function.

It provides a PHP endpoint which automatically runs `git pull` when commits are
pushed to a (preconfigured) GitLab repo, triggering a webhook. The project can
be configured to work for any number of repos and branches hosted on one server.
It also provides a PHP endpoint for viewing basic status information about the
various configured branches.

This project only runs `git pull`, and is therefore supposed to be used for
things like PHP websites, which don't require any build scripts to be ran before
being used in production.

Note that this project does not provide any authentication. By default, the
dashboard pages are public. However, no configuration settings can be changed
from the dashboard. You can secure the dashboard using authentication methods
built in your webserver. You can also fully disable the dashboard using the configuration of this project (see below).

## Installation

You need a PHP server to run this project. Just clone this repo to any desired
directory, making sure it is internet-accessible:

    $ git clone https://gitlab.com/lkkmpn/gitlab-webhook-php.git

Make sure that the repo is accessible to the user running the web server (e.g.
`www-data`).

## Configuration

### Setting up the server side

**PHP config**

The configuration for the webhook is set in `config.php`. This file is not
present by default. However, an example file is included in this repo. To copy
this example file, execute the command:

    $ cp config.example.php config.php

This file contains three variables, `$branches`, `$secret_token`, and `$enable_dashboard`.

Whenever a commit is pushed to any of the branches configured in `$branches`
(and the webhook is correctly set up on GitLab, see below), this branch will
automatically be pulled on the server. All branches not included in this array
will be ignored by the webhook.

Every branch in `$branches` should be an array with three keys:

* `repo`: the full path name of the repository (namespace and name), e.g.
  `lkkmpn/gitlab-webhook-php`.
* `branch`: the name of the branch, e.g. `master`.
* `dir`: the directory on the server in which the repo should reside, e.g.
  `/var/www/gitlab-webhook-php`.

The secret token configured in `$secret_token` is used to validate received
payloads, and should be equal to the secret token configured on GitLab when
adding the webhook. If you do not want to use a secret token, you can remove
this variable or set it to `false`. Note, however, that anyone can then trigger
a Git pull on your server, given that they know the webhook URL endpoint.

`$enable_dashboard` can be used to enable or disable the dashboard pages
(`index.php` and `status.php`). Since no authentication is built into this
project, this can be used to prevent unwanted access to the dashboard pages.
Note that all dashboard pages are read-only, so no changes can be made, but it
might still be desirable to prevent access since some file names are exposed.
Alternatively, you can prevent access using authentication methods built in to
your webserver, e.g. using HTTP Basic Authentication
([Apache](https://httpd.apache.org/docs/2.4/howto/auth.html) and
[nginx](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)
documentation articles). Make sure `webhook.php` stays accessible, though, otherwise GitLab cannot access the webhook endpoint!

**Git config**

All repos configured in the webhook must be present on the server before they
can be pulled automatically (in other words, cloning the repo is not done
automatically). In order to clone a repo on the server, execute the following
command in the directory configured in `dir`:

    $ git clone -b [branch] --single-branch [url] .

where `[branch]` and `[url]` are replaced by the branch name and the GitLab repo
url, respectively. To make sure permissions are correct after cloning, it is
recommended to run this command as the user which is also running the web
server. This can be done using `sudo` (if the current user has the correct
`sudoers` permissions), for example if the user is `www-data`:

    $ sudo -u www-data git clone -b [branch] --single-branch [url] .

It is recommended to use and configure an SSH deploy key for your server.
Details for this can be found on the ["GitLab and SSH
keys"](https://gitlab.com/help/ssh/README) help page.

### Setting up the GitLab side

You need to point GitLab to the webhook for every repo configured above,
otherwise GitLab won't send a request to the webhook, and it cannot
automatically pull.

For this, go to Settings &rarr; Integrations on the GitLab page for the repo,
and add the webhook here. Fill in the form as follows:

* URL: the internet-accessible URL of this project, followed by `webhook.php`.
  Example: `https://example.com/webhook.php`.
* Secret Token: the secret token set in `config.php` (the value of
  `$secret_token`).
* Trigger: for this webhook, "push events" is enough.
* SSL verification: if you are hosting this project on an SSL-enabled server,
  tick the box.

More information on GitLab webhooks can be found on the [GitLab webhooks help page](https://gitlab.com/help/user/project/integrations/webhooks).

## License

This project is licensed under [the MIT license](LICENSE.md).
